#include <GLFW/glfw3.h>
#include <cmath>
#include <iostream>
#include <stdio.h>

#define PI 3.14159265359

// ############# STRUCT #############

typedef struct { GLdouble x, y, z; } VECTOR3;
typedef struct { GLdouble x, y, z, w; } VECTOR4;
typedef GLdouble MATRIX4[4][4];

// ############# GLOBAL VARIABLES #############

double r_alpha = 0.0; // rotate matrix alpha
double upup = 0.0; // moving up
double zoom = 37.0; // starting zoom size

GLdouble winWidth = 800.0f;
GLdouble winHeight = 600.0f;

MATRIX4 view; 	 		// camera's matrix
MATRIX4 Vc;			 		// central projection's matrix
MATRIX4 Wc;			 		// window to viewport's matrix
MATRIX4 Vc_view; 		// Vc * view
MATRIX4 Wc_Vc_view; // Wc * Vc * view

MATRIX4 rotateX; // rotate around X axis
MATRIX4 rotateY; // rotate around Y axis
MATRIX4 rotateZ; // rotate around Z axis

MATRIX4 scaleMatrix; // scale Matrix for cubes

// move matrices for cubes
MATRIX4 Move_1;
MATRIX4 Move_2;
MATRIX4 Move_3;
MATRIX4 Move_4;

MATRIX4 torusMoveUp; // the move matrix of the toruses

GLdouble alpha = 0.0;
GLdouble deltaAlpha = (PI / 180.0f) * 3;

GLdouble center = 6.0f; // central projection's center

// camera vectors
VECTOR3 eye;
VECTOR3 up;
VECTOR3 centerVec;

// ############# FUNCTION DECLARATIONS #############

VECTOR3 initVector3( GLdouble, GLdouble, GLdouble );
VECTOR4 initVector4( GLclampd, GLclampd, GLclampd, GLclampd );
VECTOR3 convertToInhomogen( VECTOR4 );
VECTOR4 convertToHomogen( VECTOR3 );
GLdouble getVectorLength( VECTOR3 );
VECTOR3 normalizeVector( VECTOR3 );
GLdouble dotProduct( VECTOR3, VECTOR3 );
VECTOR3 crossProduct( VECTOR3, VECTOR3 );
void initIdentityMatrix( MATRIX4 );
void initPersProjMatrix( MATRIX4, GLdouble );
void initRotateMatrixX( MATRIX4, GLdouble );
void initRotateMatrixY( MATRIX4, GLdouble );
void initRotateMatrixZ( MATRIX4, GLdouble );
void initMoveMatrix( MATRIX4, VECTOR3 );
void initScaleMatrix( MATRIX4, double, double, double );
void initWtvMatrix( MATRIX4, double, double, double, double,
														 double, double, double, double );
void initViewMatrix( MATRIX4, VECTOR3, VECTOR3, VECTOR3 );
VECTOR4 mulMatrixVector( MATRIX4, VECTOR4 );
void mulMatrices( MATRIX4, MATRIX4, MATRIX4 );
void init();
void initTransformations();
void drawCube( MATRIX4, MATRIX4 );
void drawFence( VECTOR3, MATRIX4 );
void keyPressed( GLFWwindow*, GLint, GLint, GLint, GLint );

// ############# TORUS CLASS #############

class Torus
{
private:
	double R; // R is the distance from the center of the tube to the center of the torus,
	double r; // r is the radius of the tube.
public:

	Torus( double R, double r )
	{
		this->R = R; // the radius of the bigger circles
		this->r = r; // the radius of the small circles
	}

	void drawTorus( MATRIX4 T, MATRIX4 rotate, MATRIX4 move, VECTOR3 color )
	{
		// T = Wc * Vc * view
		glLineWidth(2.0f);

		VECTOR4 ph, pt;
		VECTOR3 pih;

		MATRIX4 move_rotate;
		mulMatrices( move, rotate, move_rotate);
		// move_rotate = torusMoveUp * rotateX || rotateY || rotateZ

		MATRIX4 T_move_rotate;
		mulMatrices( T, move_rotate, T_move_rotate );
		// T_move_rotate = Wc * Vc * view * torusMoveUp * rotateX || rotateY || rotateZ

		glColor3f( color.x, color.y, color.z );

		// big circles
		for( double theta = 0; theta <= 2*PI+0.001; theta += PI/6 )
		{
			glBegin(GL_LINE_STRIP);

		  for( double phi = 0; phi <= 2*PI+0.001; phi += PI/16 )
		  {
				ph = initVector4( (R+r*cos(theta))*cos(phi), r*sin(theta), (R+r*cos(theta))*sin(phi), 1.0 );
				pt = mulMatrixVector(T_move_rotate, ph);
				// T_move_rotate = Wc * Vc * view * torusMoveUp * rotateX || rotateY || rotateZ
		    pih = convertToInhomogen( pt );

				glVertex2f(pih.x, pih.y);
			}

	    glEnd();
		}

		// small circles
		for( double phi = 0; phi <= 2*PI+0.001; phi += PI / 16 )
	  {
			glBegin(GL_LINE_STRIP);

			for( double theta = 0; theta <= 2*PI+0.001; theta += PI / 6 )
	    {
				ph = initVector4( (R+r*cos(theta))*cos(phi), r*sin(theta), (R+r*cos(theta))*sin(phi), 1.0 );
	      pt = mulMatrixVector( T_move_rotate, ph );
				// T_move_rotate = Wc * Vc * view * torusMoveUp * rotateX || rotateY || rotateZ
	      pih = convertToInhomogen( pt );

				glVertex2f(pih.x, pih.y);
			}
			glEnd();
		}
	}
};

int main()
{
	GLFWwindow* window;

	// Initialize the library
	if( !glfwInit() )
		return -1;

	// Create a windowed mode window and its OpenGL context
	window = glfwCreateWindow( winWidth, winHeight, "Third TorusCamera", NULL, NULL );
	if( !window )
	{
		glfwTerminate();
		return -1;
	}

	// Make the window's context current
	glfwMakeContextCurrent( window );

	glfwSetKeyCallback( window, keyPressed );

	init();

	Torus green = Torus(8.0, 1.0);
	Torus red = Torus(8.0, 1.0);
	Torus blue = Torus(8.0, 1.0);

	initMoveMatrix( Move_1, initVector3( -9.5, 9.0, -9.5 ) );
	initMoveMatrix( Move_2, initVector3( -9.5, 9.0, 9.5 ) );
	initMoveMatrix( Move_3, initVector3( 9.5, 9.0, -9.5 ) );
	initMoveMatrix( Move_4, initVector3( 9.5, 9.0, 9.5 ) );

	// Loop until the user closes the window
	while( !glfwWindowShouldClose( window ) )
	{
		glClear(GL_COLOR_BUFFER_BIT);

		initRotateMatrixX( rotateX, r_alpha );
		initRotateMatrixY( rotateY, r_alpha );
		initRotateMatrixZ( rotateZ, r_alpha );

		// drawing fence
		drawFence( initVector3(0.6f, 0.6f, 0.6f), Wc_Vc_view );

		// tóruszok rajzolása
		green.drawTorus( Wc_Vc_view, rotateX, torusMoveUp, initVector3(0.0f, 1.0f, 0.0f) );
		red.drawTorus( Wc_Vc_view, rotateY, torusMoveUp, initVector3(1.0f, 0.0f, 0.0f) );
		blue.drawTorus( Wc_Vc_view, rotateZ, torusMoveUp, initVector3(0.0f, 0.0f, 1.0f) );
		r_alpha -= 0.1;

		// draw cubes
		drawCube( Wc_Vc_view, Move_1 );
		drawCube( Wc_Vc_view, Move_2 );
		drawCube( Wc_Vc_view, Move_3 );
		drawCube( Wc_Vc_view, Move_4 );

		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}

// ############# INITIALIZER AND OPERATION FUNCTIONS #############

VECTOR3 initVector3( GLdouble x, GLdouble y, GLdouble z )
{
	VECTOR3 P;
	P.x = x;
	P.y = y;
	P.z = z;
	return P;
}
VECTOR4 initVector4( GLclampd x, GLclampd y, GLclampd z, GLclampd w )
{
	VECTOR4 P;
	P.x = x;
	P.y = y;
	P.z = z;
	P.w = w;
	return P;
}
VECTOR3 convertToInhomogen( VECTOR4 vector )
{
	return initVector3(vector.x / vector.w, vector.y / vector.w, vector.z / vector.w);
}
VECTOR4 convertToHomogen( VECTOR3 vector )
{
	return initVector4(vector.x, vector.y, vector.z, 1.0);
}
GLdouble getVectorLength( VECTOR3 vector )
{
	return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}
VECTOR3 normalizeVector( VECTOR3 vector )
{
	GLdouble length = getVectorLength(vector);
	return initVector3(vector.x / length, vector.y / length, vector.z / length);
}
GLdouble dotProduct( VECTOR3 a, VECTOR3 b )
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}
VECTOR3 crossProduct( VECTOR3 a, VECTOR3 b )
{
	return initVector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x );
}
void initIdentityMatrix( MATRIX4 A )
{
	for( int i = 0; i<4; i++)
		for( int j = 0; j<4; j++)
			A[i][j] = 0.0f;

	for( int i = 0; i<4; i++)
		A[i][i] = 1.0f;
}
void initPersProjMatrix( MATRIX4 A, GLdouble s )
{
	initIdentityMatrix(A);

	A[2][2] = 0.0f;
	A[3][2] = -1.0f / s;
}
void initRotateMatrixX( MATRIX4 A, GLdouble alpha )
{
	initIdentityMatrix(A);

	A[1][1] = cos( (PI / 180) * alpha );
	A[1][2] = -1.0 * sin( (PI / 180) * alpha );

	A[2][1] = sin( (PI / 180) * alpha );
	A[2][2] = cos( (PI / 180) * alpha );
}
void initRotateMatrixY( MATRIX4 A, GLdouble alpha )
{
	initIdentityMatrix(A);

	A[0][0] = cos( (PI / 180) * alpha );
	A[0][2] = -1.0 * sin( (PI / 180) * alpha );

	A[2][0] = sin( (PI / 180) * alpha );
	A[2][2] = cos( (PI / 180) * alpha );
}
void initRotateMatrixZ( MATRIX4 A, GLdouble alpha )
{
	initIdentityMatrix(A);
	A[0][0] = cos( (PI / 180) * alpha );
	A[0][1] = sin( (PI / 180) * alpha );

	A[1][0] = -1.0 * sin( (PI / 180) * alpha );
	A[1][1] = cos( (PI / 180) * alpha );
}
void initMoveMatrix( MATRIX4 A, VECTOR3 d )
{
	initIdentityMatrix(A);
	A[0][3] = d.x;
	A[1][3] = d.y;
	A[2][3] = d.z;
}
void initScaleMatrix( MATRIX4 A, double lambda, double mu, double nu )
{
	initIdentityMatrix(A);
	A[0][0] = lambda;
	A[1][1] = mu;
	A[2][2] = nu;
}
void initWtvMatrix( MATRIX4 A, double vleft, double vbottom, double vright, double vtop,
															 double wleft, double wbottom, double wright, double wtop )
{
	initIdentityMatrix(A);
	// 0, 0, 800, 600, -3, -3, 3, 3

	A[0][0] = (vright-vleft)/(wright-wleft);
	A[1][1] = (vtop-vbottom)/(wtop-wbottom);
	A[0][3] = (vleft-wleft)*( (vright-vleft)/(wright-wleft) );
	A[1][3] = (vbottom-wbottom)*( (vtop-vbottom)/(wtop-wbottom) );
}
void initViewMatrix( MATRIX4 A, VECTOR3 eye, VECTOR3 center, VECTOR3 up )
{
	initIdentityMatrix(A);

  VECTOR3 centerMinusEye = initVector3(center.x - eye.x, center.y - eye.y, center.z - eye.z);
  VECTOR3 f = normalizeVector(initVector3( -centerMinusEye.x, -centerMinusEye.y, -centerMinusEye.z));
  VECTOR3 s = normalizeVector( crossProduct(up, f));
  VECTOR3 u = crossProduct(f, s);

  A[0][0] = s.x;
  A[0][1] = s.y;
  A[0][2] = s.z;
  A[0][3] = -dotProduct(s, eye);

  A[1][0] = u.x;
  A[1][1] = u.y;
  A[1][2] = u.z;
  A[1][3] = -dotProduct(u, eye);

  A[2][0] = f.x;
  A[2][1] = f.y;
  A[2][2] = f.z;
  A[2][3] = -dotProduct(f, eye);
}
VECTOR4 mulMatrixVector( MATRIX4 A, VECTOR4 v )
{
	return initVector4(
		A[0][0] * v.x + A[0][1] * v.y + A[0][2] * v.z + A[0][3] * v.w,
		A[1][0] * v.x + A[1][1] * v.y + A[1][2] * v.z + A[1][3] * v.w,
		A[2][0] * v.x + A[2][1] * v.y + A[2][2] * v.z + A[2][3] * v.w,
		A[3][0] * v.x + A[3][1] * v.y + A[3][2] * v.z + A[3][3] * v.w);
}
void mulMatrices( MATRIX4 A, MATRIX4 B, MATRIX4 C )
{
	GLdouble sum;
	for( int i = 0; i < 4; i++)
		for( int j = 0; j < 4; j++) {
			sum = 0;
			for( int k = 0; k < 4; k++)
				sum = sum + A[i][k] * B[k][j];
			C[i][j] = sum;
		}
}

// ############# GLOBAL INITIALIZATIONS ############

VECTOR4 identityCube[8] =
{
	initVector4(0.5f, 0.5f,-0.5f, 1.0f),
	initVector4(-0.5f, 0.5f,-0.5f, 1.0f),
	initVector4(-0.5f,-0.5f,-0.5f, 1.0f),
	initVector4(0.5f,-0.5f,-0.5f, 1.0f),
	initVector4(0.5f, 0.5f, 0.5f, 1.0f),
	initVector4(-0.5f, 0.5f, 0.5f, 1.0f),
	initVector4(-0.5f,-0.5f, 0.5f, 1.0f),
	initVector4(0.5f,-0.5f, 0.5f, 1.0f),
};

GLuint faces[24] =
{
	0,1,2,3, // bottom
	0,1,5,4, // right
	1,2,6,5, // back
	2,3,7,6, // left
	3,0,4,7, // front
	4,5,6,7, // up
};

// ############# FUNCTION DEFINITIONS ############

void init()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// left, right, bottom, top
	glOrtho(0.0f, winWidth, 0.0f, winHeight, 0.0f, 1.0f);

	// gives the camera position ( x axis positive side )
	eye = initVector3( 37.0, 0.0, 0.0 );
	// gives the look of the camera ( origo )
	centerVec = initVector3( 0.0, 0.0, 0.0 );
	// gives that where is the up ( y axis )
	up = initVector3( 0.0, 1.0, 0.0 );

	// the camera's coordinate system
	initViewMatrix(view, eye, centerVec, up);

	initTransformations();
}

void initTransformations()
{
	initMoveMatrix( torusMoveUp, initVector3( 0.0, 9.0, 0.0 ) );
	initScaleMatrix( scaleMatrix, 5, 18, 5 );

	// centralprojection Matrix
	initPersProjMatrix(Vc, center);

	// window to viewport matrix

	double viewLeft = (winWidth - winHeight) / (winHeight / 4); // => 4: the window width
	// winWidth - winHeight => the difference between the width and height
	// (800 - 600) / ( 600 / 4 );
	// 		200 		 / 		 150
	// 					1.33333

	// vleft, vbottom, vright, vtop,
	// wleft, wbottom, wright, wtop
	initWtvMatrix( Wc, viewLeft, 0.0, 600.0, 600.0,
										 -4.0,    -4.0, 4.0,   4.0 );

	// matrix multiplies
	mulMatrices(Vc, view, Vc_view);
	mulMatrices(Wc, Vc_view, Wc_Vc_view);
	// Wc_Vc_view = Wc * Vc * view
}

void drawFence( VECTOR3 color, MATRIX4 T )
{
	// window to viewport, central projection, camera
	// T =  Wc * Vc * view

	VECTOR4 ph, pt;
	VECTOR3 pih;

	glLineWidth(2.0f);

	glColor3f(color.x, color.y, color.z);

	for( int j = -12; j <= 12; ++j )
	{
		glBegin(GL_LINES);

		// parallel with x axis
		// from (12,0,-12) to (12,0,12)
		ph = initVector4(12, 0, j, 1.0f);
		pt = mulMatrixVector(T, ph);
		// T =  Wc * Vc * view
		pih = convertToInhomogen( pt );
		glVertex2f(pih.x, pih.y);

		// from (-12,0,-12) to (-12,0,12)
		ph = initVector4(-12, 0, j, 1.0f);
		pt = mulMatrixVector(T, ph);
		// T =  Wc * Vc * view
		pih = convertToInhomogen( pt );
		glVertex2f(pih.x, pih.y);

		// parallel with z axis
		// from (-12,0,12) to (12,0,12)
		ph = initVector4(j, 0, 12, 1.0f);
		pt = mulMatrixVector(T, ph);
		// T =  Wc * Vc * view
		pih = convertToInhomogen( pt );
		glVertex2f(pih.x, pih.y);

		// from (-12,0,-12) to (12,0,-12)
		ph = initVector4(j, 0, -12, 1.0f);
		pt = mulMatrixVector(T, ph);
		// T =  Wc * Vc * view
		pih = convertToInhomogen( pt );
		glVertex2f(pih.x, pih.y);

		glEnd();
	}
}

void drawCube( MATRIX4 T, MATRIX4 move )
{
	// windowtoviewport, central projection, camera
	// T =  Wc * Vc * view
	int id = 0;

	VECTOR4 ph, pt;
	VECTOR3 pih;

	glLineWidth( 2.0f );
	glColor3f( 0.0, 0.0, 0.0 );

	MATRIX4 move_scaleMatrix;
	mulMatrices( move, scaleMatrix, move_scaleMatrix );
	// move_scaleMatrix = move * scaleMatrix

	MATRIX4 T_move_scaleMatrix;
	mulMatrices( T, move_scaleMatrix, T_move_scaleMatrix );
	// T_move_scaleMatrix = Wc * Vc * view * move * scaleMatrix

	// six faces
	for( int i = 0; i < 6; ++i )
	{
		glBegin(GL_LINE_LOOP);
		// on a face four vertices
		for( int j = 0; j < 4; ++j )
		{
			ph = initVector4( identityCube[faces[id]].x, identityCube[faces[id]].y, identityCube[faces[id]].z, 1.0 );
			pt = mulMatrixVector( T_move_scaleMatrix, ph );
			// T_move_scaleMatrix = Wc * Vc * view * move * scaleMatrix
			pih = convertToInhomogen( pt );

			glVertex2f(pih.x, pih.y);
			id++;
		}
		glEnd();
	}
}

void keyPressed(GLFWwindow * windows, GLint key, GLint scanCode, GLint action, GLint mods)
{
	if (action == GLFW_PRESS || GLFW_REPEAT)
	{
		switch (key)
		{
		case GLFW_KEY_LEFT:
			alpha += deltaAlpha;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_RIGHT:
			alpha -= deltaAlpha;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_UP:
			upup+=1.0;
			eye.y = upup;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_DOWN:
			upup-=1.0;
			eye.y = upup;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_KP_SUBTRACT:
			zoom += 0.7;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_KP_ADD:
			if( zoom  > 1e-15 )
				zoom -= 0.7;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		}
	}
	glfwPollEvents();
}
